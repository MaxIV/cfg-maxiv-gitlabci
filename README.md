# cfg-maxiv-gitlabci

## Description

YML templates for GitLab CI / CD

## Usage

Used as a base reference for all (at least most) of our gitlab projects.

In order to use this template you need to create a `.gitlab-ci.yml` inside your project
repository. As and example you can use the following code:

### Python

```yml
include:
  - project: 'kits-maxiv/cfg-maxiv-gitlabci'
    file: '/.python-ci.yml'

```

If your project includes a `.pre-commit-config.yaml` file, a job will run [pre-commit](https://pre-commit.com)
on all your files.

You can also use the `FPM_FLAGS` variable to specify others fpms flags.

The current list of build job supported:

* build-pypi-package
* build-conda-package
* auto-build-conda-package
* build el6 python 26
* build el7 python 27
* build el7 python 34
* build el7 python 36

To publish a package to the production repository (pypi, conda or RPM), you should tag the default branch:

```bash
git tag -a 1.2.0 -m "release 1.2.0"
git push --tags
```

You can also do that from the GitLab UI under the Repository->Tags menu.
Make sure to enter a message to not create a lightweight tag.

During development, you can publish a package to the test repository by triggering the manual job
or by adding `[publish]` to your commit message.
This can be done from any branch, except the default one (`master` or `main`).

Python packages are pushed to:

* `https://artifactory.maxiv.lu.se/artifactory/pypi-maxiv-test/`: test repository
* `https://artifactory.maxiv.lu.se/artifactory/pypi-maxiv-stable/`: production/stable repository

[Artifactory](https://artifactory.maxiv.lu.se/ui/repos/tree) is the official repository.

**By default RPM package are now disabled**. To enable RPM build, set the variable `RPM_ENABLED` to `true`.
You can disable the build of a Python package (if you only want to build a RPM) by setting the
variable `PYPI_ENABLED` to `false`.

The pipeline will try to automatically build a conda package by creating a recipe using [grayskull](https://github.com/conda-incubator/grayskull) and [rattler-build](https://github.com/prefix-dev/rattler-build). If the package isn't `noarch`, that will fail.
This can happen if you have defined some `scripts` in your `setup.py`. Check if they can be replaced with an `entry_point`.
By default, when an entry point is present, a test to run it with the `--help` flag is added to the recipe. This test is only removed if taurus is defined in the recipe as taurus GUIs don't have such a flag.
As RPM are disabled by default, this job isn't allowed to fail.
It can be disabled by setting the variable `CONDA_AUTO_PACKAGING_ENABLED` to `false`.

For more complex packages or if you want more control, you can create your own recipe.
It should be defined in the `recipe/meta.yaml` file. `grayskull` won't be used in that case.
The conda package can be uploaded to the `maxiv-kits-dev` conda channel from any branch, except the default one, by triggering the manual job.
Tagging the repository will publish the package to the `maxiv-kits` channel.

To define the dependencies (build, test and installation) you should update your setup.py like this:

```python
from setuptools import setup, find_packages

tests_require = ["pytest", "pytest-cov", "pkg2"]

setup(
    name = "tangods-example",
    version = "1.0.0",
    description = "Setup.py example file.",
    author = "KITS - Controls",
    author_email = "KITS@maxiv.lu.se",
    license = "GPLv3",
    url = "http://www.maxiv.lu.se",
    packages = find_packages(),
    provides = ["example"],
    install_requires = ['example_pkg1', 'example_pkg3'],
    entry_points={'console_scripts': ['Example = example:main']},
    extras_require={"tests": tests_require},
)
```

The `tests` key defined in `extra_requires` will allow you to install
the tests requirements using `pip install .[tests]`.

When building RPM, FPM will automatically add `python` (variate with python version) as a prefix to your requires list, install-requires list and package name.

**WARNING**: Be careful to use the proper Python package name in `install_requires`!
Sometimes the python package name doesn't match the import name. For example:

* the _dateutil_ Python module package is called [python-dateutil](https://pypi.org/project/python-dateutil/)
* the _vxi11_ Python module package is called [python-vxi11](https://pypi.org/project/python-vxi11/)

When not sure about a name, check <https://pypi.org>.

Also note that pip isn't case sensitive, but yum is. Defining `pyyaml` in `install_requires` will
work when installing the package via pip but not when building a RPM (`python36-pyyaml` will not be found). Make sure to use the name with the proper casing: `PyYAML` in this case.
To ensure that the case defined in `install_requires` is respected by fpm, the `--no-python-downcase-dependencies` fpm flag shall be used, which is done by default.

By default only **python3.6** RPM is built. To enable other builds, override the `PYTHON_VERSIONS` variable (note that it is a regex):

```yml
variables:
  RPM_ENABLED: "true"
  PYTHON_VERSIONS: '/python2.7|python3.6/'
```

If the variable `PACKAGE_NAME` isn't set, all RPM builds will be published (the package name will be prefixed with the python version).
If `PACKAGE_NAME` is set and several python versions are built (using `PYTHON_VERSIONS`), only the build linked to `NAMED_PACKAGE_PYTHON_VERSION` will be published (default to `python3.6`).
If only one python version is built, the `NAMED_PACKAGE_PYTHON_VERSION` is ignored.

### Inventory update

When tagging a repository, the inventory-update script will try to update the default version in the `versions` dict under `inventory/group_vars/all.yml`.

A MR is only created if that package is already defined and the version is different. Nothing is done if the package isn't defined in that file.

The MR is merged on success only when automatic deployment is setup (checking the `DEPLOY_HOSTS` variable). See below.
Manual merge is required otherwise.

### Automatically deploy on tag

It is possible to **automatically deploy on tag**. This can be done for example in case of a beamline synoptic. Currently, it is possible to update a conda environment and to deploy an RPM. The `DEPLOY_HOSTS` variable should be specified in the Gitlab CI to determine on which hosts the deployment should happen.

To deploy a package in a conda environment, it should be set-up in the `cfg-maxiv-ansible` repository, an example for dummymax can be found [here](https://gitlab.maxiv.lu.se/kits-maxiv/ansible-galaxy/cfg-maxiv-ansible/-/blob/master/inventory/group_vars/dummymax-cc.yml). The `DEPLOY_CONDA_ENV_NAME` variable should be specified in the Gitlab CI with the same name as the `env_name` in configuration.
This is only for the `conda_envs` defined in tango-client (cc) hosts.

To deploy a tango device server defined with the `tango_ds` variable (ec hosts), use the `DEPLOY_TANGO_DS_NAME` variable in the repository Gitlab CI file.
That variable should match the tangods `name` specified in the Ansible inventory for `tango_ds` entry point. Note that is not the `name` for `instances` or `devices` variable.

To deploy an RPM package, it should be defined under `packages_stable` in the `cfg-maxiv-ansible` repository. The `DEPLOY_PACKAGE_NAME` variable should be specified in the Gitlab CI with the same name as in the ansible configuration.

In both cases, the version of the package should be set to `default`, meaning that it should be defined in the `inventory/group_vars/all.yml` file. If the version is pinned, the package will not be updated!
Note that it was possible to use `latest` before but this isn't recommended anymore. It's deprecated in favor of `default` now
that the `inventory-update` exists.

#### Testing

Following jobs are defined to run tests:

* test-python27 (off by default)
* integration-test-python37 (off by default)
* test-python36 (on by default)
* integration-test-python36 (off by default)
* test-python39 (on by default)
* integration-test-python39 (off by default)
* test-python310 (on by default)
* integration-test-python310 (off by default)

A test stage will automatically try to run `pytest` if some tests are found (`test_*.py` under any directory). You can pass extra arguments to `pytest` using the `PYTEST_EXTRA_ARGS` variable.
The default arguments used are `-v -m "not tangodb" --cov=$MODULE_NAME`. `MODULE_NAME` is automatically set to the project name with `-` replaced by `_`. Override it if needed.

Don't forget to add your tests dependencies using `extras_require={"tests": ..` in your `setup.py`.

The default image used is `harbor.maxiv.lu.se/kits-sw/python-testing:3.6` and it is preinstalled with PyTango.

If testing requires other than python packages, conda `environment.yml` file can be added in the project which will be used by the CI to create the environment in which the tests will be run.

If you need extra steps to setup your test, you can create the `.ci/before-test` script.
It will be executed before running `pytest`.

The `test-python36` job is meant to run unit tests.
If you want to run integration tests that require a tango database, you can enable the `integration-test-python36` job by setting the variable `INTEGRATION_TEST_ENABLED: "true"`.
You have to mark those tests with `@pytest.mark.tangodb` so that they are only run by this job.

If you only have integration tests, you could skip the unit test by setting `UNIT_TEST_ENABLED: "false"`.

Tests are also run for Python 3.9 3.10 and 3.11.
This is to check compatibility with newer Python versions.
We already run 3.9 when deploying with conda.

The jobs with Python 3.11 are allowed to fail.
If you can't fix the tests on another Python version due to some dependencies, you can use:

```yaml
test-python310:
   allow_failure: true
```

By default, tests are run on Python 3 only.
To run tests on Python 2, use `TEST_PYTHON_VERSIONS: '/python2.7/'`. To run on both Python 2 and 3, use:

```yml
variables:
  TEST_PYTHON_VERSIONS: '/python2.7|python3.6/'
```

Note that Python 3.9 and 3.10 will also be run when specifying 3.6.

### Python Tango Device

The `.pythonds-ci.yml` template is deprecated. It's equivalent to the the `.python-ci.yml` file.

For tango devices you will need to define the `PACKAGE_NAME` variable **if** you want to build a RPM. This variable will be the name of your RPM package.
If you don't set `RPM_ENABLED`, only pypi and conda packages will be built. You don't have to specify anything extra.

```yml
include:
  - project: 'kits-maxiv/cfg-maxiv-gitlabci'
    file: '/.python-ci.yml'

# Only needed if you want to build a RPM
variables:
  RPM_ENABLED: "true"
  PACKAGE_NAME: 'tangods-example'
```

You can also use `FPM_FLAGS` and other variables like for any Python projects.

If several python versions are built (using `PYTHON_VERSIONS`), only the build linked to `NAMED_PACKAGE_PYTHON_VERSION` will be published (default to `python3.6`).

### Python staging

To build from a remote repo you can make a staging repo, and add the REMOTEREPO varable and optionally specify a REF.

This should be considered deprecated. The recommended way is to submit the package to conda-forge if not already available.
See <https://github.com/conda-forge/staged-recipes>.

```yml
build el7 python 27:
  variables:
    DEPLOY: 'True'
    REMOTEREPO: 'https://github.com/Something/somerepo.git'
    REF: '123456789abcdef'
```

### C++

```yml
include:
  - project: 'kits-maxiv/cfg-maxiv-gitlabci'
    file: '/.cpp-ci.yml'
```

In order to use this template you need a .spec file in order for maxpkg to build and package your project.

You can also decide to disable some combinaison of builds by overiding the build job and by defining a variable UNSUPPORTED, i.e:

```yml
build el7 i386:
  variables:
    UNSUPPORTED: 'True'

```

The current list of build job supported:

* build el7 x86_64
* build el6 x86_64
* build el6 i386

The current list of build job but disable by default:

* build el7 i386

### Docker

If you have a `Dockerfile` at the root of your project, you can use the `Docker.gitlab-ci.yml`
template to build and push the docker image.

```yml
include:
  - project: 'kits-maxiv/cfg-maxiv-gitlabci'
    file: '/Docker.gitlab-ci.yml'
```

By default, the image will have the same name as the GitLab project.
The docker image is built and released with the name of the branch as tag on every push.
When pushing a git tag, the *tag* is used as image tag and *latest* is also released.

The following variables can be used to change the default behaviour:

* `DOCKER_REGISTRY`: name of the docker registry [default: docker.maxiv.lu.se]
* `IMAGE_NAME`: name of the docker image [default: $CI_PROJECT_NAME]
* `MAJOR_TAG`: extra tag to release [default: ""]

For a docker image including ansible for example, it would make sense to tag with the version
of ansible installed in the image (like 2.9.17).
The `MAJOR_TAG` variable could be used to also tag the latest *2.9.x* image with *2.9*.

`DOCKER_REGISTRY` can be set to a harbor project, i.e. `harbor.maxiv.lu.se/kits-sw`.
In this case, `DOCKER_REGISTRY_LOGIN` and `DOCKER_REGISTRY_PASSWORD` must be set in the CI/CD
variables at the group or project level. Those variables are defined for the
[docker-base-images](https://gitlab.maxiv.lu.se/kits-maxiv/docker-base-images) group.

Never define directly the password in the `.gitlab-ci.yml` file!

### Molecule

The `Molecule.gitlab-ci.yml` template shall be used to test all Ansible roles and playbooks.
It is automatically included in the cookiecutter template to create a new [role](https://gitlab.maxiv.lu.se/kits-maxiv/cookiecutters/cookiecutter-kits-ans-role).

### How to update a template

Each commit will automatically trigger the gitlab template to build dummy projects, in order to validate the templates.

* [x]  Python template
* [x]  Pythonds template
* [x]  C++ template
* [x]  Python staging template

OBS:

*  The pipeline is automatically triggered when pushing to a branch
*  The pipeline should always fail in the publish job, once that the package already exist.
*  Tokens were saved as CI env variables.
*  New suggestions are welcome!

Repo links:

*  https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-gitlabcipython
*  https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-gitlabcipythonds
*  https://gitlab.maxiv.lu.se/kits-maxiv/lib-maxiv-gitlabcicpp
*  https://gitlab.maxiv.lu.se/kits-maxiv/pyjokes-staging

#### Define more platform to build

In case you want to add new platform you should add new build job if the current template is compatible. Also don't forget to add as many build as many publishing triggers. The publish job is generic enough for the RPM based system.

### Tango Catalogue

The repository contains **.dsc-update.yml** file compatible with GiLab CI which allows
integrating with the device classes catalogue (DSC) through GitLab CI

The **.dsc-update.yml** file contains:

* variables needed for the catalogue update which are common for all device repositories
* docker image with Tango CS Device Servers Catalogue import utility
* invocation of an import utility script

#### How to use

The `dsc-update` job has been included  in `.python-ci.yml`, `.pythonds.yml` and `.cpp-ci.yml`. In order to run that job,
in repository with Tango device, you need to specify variables related to specific
device server. The DSC_DEVICE_SERVER_NAME is already defined in `.dsc-update.yml` file (based on project name),
but can be also overridden. For example:

```yml
include:
  - project: 'kits-maxiv/cfg-maxiv-gitlabci'
    file: '/.pythonds-ci.yml'

variables:
  DSC_XMI_URL: "file://$CI_PROJECT_DIR/src/$DSC_DEVICE_SERVER_NAME.xmi"
  DSC_FAMILY: 'Vacuum'
  DSC_CLASS_DESCRIPTION: "Modulator from SCANDINOVA"
```

## Links

Gitlab CI documentation:

* https://docs.gitlab.com/ee/ci/

FPM repo:

* https://github.com/jordansissel/fpm

FPM wiki with all tags:

* https://github.com/jordansissel/fpm/wiki

maxpkg repo:

* https://gitlab.maxiv.lu.se/kits-maxiv/app-maxiv-maxpkg

## Authors

KITS SW group

kitscontrols@maxiv.lu.se
